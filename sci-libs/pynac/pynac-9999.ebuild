# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"

PYTHON_DEPEND="2:2.7:2.7"

inherit autotools-utils python mercurial

DESCRIPTION="A modified version of GiNaC that replaces the dependency on CLN by Python"
HOMEPAGE="http://pynac.sagemath.org/ https://bitbucket.org/burcin/pynac/overview"
EHG_REPO_URI="http://bitbucket.org/burcin/pynac"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="static-libs"

DEPEND="dev-util/pkgconfig"
RDEPEND=""

DOCS=( AUTHORS NEWS README )

# run autotools in the source directory
AUTOTOOLS_IN_SOURCE_BUILD=1

pkg_setup() {
	# This version will use python-2.7
	python_set_active_version 2.7
	python_pkg_setup
}

src_prepare() {
	eautoreconf
}

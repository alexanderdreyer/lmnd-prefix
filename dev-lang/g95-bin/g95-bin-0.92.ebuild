# Copyright 2007 F. Bissey
# Distributed under the terms of the GNU General Public License v2
# $Header: initial work 30/12/07 $

DECRIPTION="g95 fortran compiler"
HOMEPAGE="http://g95.org/"
LICENSE="GPL"
SLOT="0"
KEYWORDS="~x86 ~amd64 ~ppc ~ppc64 ~alpha ~ia64 ~x86-linux ~amd64-linux ~ppc-macos ~x86-macos"

BASE="http://ftp.g95.org/v${PV}/g95-"
SRC_URI="x86? ( ${BASE}x86-linux.tgz )
   amd64? ( ${BASE}x86_64-64-linux.tgz )
   ppc? ( ${BASE}powerpc-32-linux.tgz )
   ppc64? ( ${BASE}powerpc-64-linux.tgz )
   alpha? ( ${BASE}alpha-64-linux.tgz )
   ia64? ( ${BASE}ia64-64-linux.tgz )
   x86-linux? ( ${BASE}x86-linux.tgz )
   x86-macos? ( ${BASE}x86-osx.tgz )
   amd64-linux? ( ${BASE}x86_64-64-linux.tgz )
   ppc-macos? ( ${BASE}powerpc-osx.tgz )
   ia64-linux? ( ${BASE}ia64-64-linux.tgz )"

S=${WORKDIR}/g95-install

src_install() {
   dodir /opt/${PN}

   cp -pR "${S}"/* "${ED}/opt/${PN}/" || die "cannot copy files to destination"

   dodir /usr/bin
   # there is only one file under /opt/${PN}/bin
   BINNAME=$(ls "${ED}/opt/${PN}"/bin/)
   dosym ../opt/${PN}/bin/$BINNAME /usr/bin/g95
 }
